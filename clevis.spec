Name:          clevis
Version:       21
Release:       1
Summary:       A plugable framework for automated decryption

License:       GPL-3.0-or-later
URL:           https://github.com/latchset/%{name}
Source0:       https://github.com/latchset/%{name}/releases/download/v%{version}/%{name}-%{version}.tar.xz
Source1:        clevis.sysusers

Patch0001:      0001-PKCS-11-pin-fix-dracut-for-unconfigured-device.patch

BuildRequires: meson
BuildRequires: pkgconfig(audit) >= 2.7.8
BuildRequires: pkgconfig(bash-completion)
BuildRequires: pkgconfig(dracut)
BuildRequires: pkgconfig(gio-2.0)
BuildRequires: pkgconfig(jansson) >= 2.10
BuildRequires: pkgconfig(jose) >= 8
BuildRequires: pkgconfig(libcrypto)
BuildRequires: pkgconfig(libcryptsetup) >= 2.0.2
BuildRequires: pkgconfig(luksmeta) >= 8
BuildRequires: pkgconfig(systemd)
BuildRequires: pkgconfig(udisks2)
BuildRequires: /usr/bin/a2x
BuildRequires: /usr/bin/curl
BuildRequires: /usr/bin/git
BuildRequires: /usr/bin/jq
BuildRequires: /usr/bin/keyctl
BuildRequires: /usr/bin/pkcs11-tool
BuildRequires: /usr/bin/tpm2_create
BuildRequires: /usr/bin/tpm2_createpolicy
BuildRequires: /usr/bin/tpm2_createprimary
BuildRequires: /usr/bin/tpm2_flushcontext
BuildRequires: /usr/bin/tpm2_load
BuildRequires: /usr/bin/tpm2_pcrread
BuildRequires: /usr/bin/tpm2_unseal
BuildRequires: /usr/sbin/cryptsetup
BuildRequires: /usr/sbin/pcscd
BuildRequires: desktop-file-utils

Requires:      tpm2-tools jose curl coreutils cryptsetup luksmeta
Provides:      clevis-luks = %{version}-%{release}
Obsoletes:     clevis-luks < %{version}-%{release}
Requires(pre): shadow-utils

%description
Clevis is a plugable framework for automated decryption. It can be used 
to provide automated decryption of data or even automated unlocking of 
LUKS volumes.

This package allows users to bind a LUKS volume using a pin so that it can
be automatically unlocked. Upon successful completion of binding, the disk
can be unlocked using one of the provided unlockers.

%package systemd
Summary:       Systemd integration for clevis
Requires:      %{name}%{?_isa} = %{version}-%{release}
Requires:      systemd

%description systemd
The systemd unlocker attempts to automatically unlock LUKSv1 _netdev block devices from /etc/crypttab.

%package dracut
Summary:       Dracut integration for clevis
Requires:      %{name}-systemd%{?_isa} = %{version}-%{release}
Requires:      dracut-network

%description dracut
The dracut unlocker attempts to automatically unlock volumes during early boot. 

%package udisks2
Summary:       Udisks2 integration for clevis
Requires:      %{name}%{?_isa} = %{version}-%{release}

%description udisks2
The udisks2 unlocker attempts to automatically unlock volumes in desktop environments 
that use UDisks2 or storaged (like GNOME). 

%package pin-pkcs11
Summary:        PKCS#11 for clevis
Requires:       %{name}-systemd%{?_isa} = %{version}-%{release}
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       %{name}-dracut%{?_isa} = %{version}-%{release}
Conflicts:      %{name} < 21
Conflicts:      %{name}-systemd < 21
Conflicts:      %{name}-dracut < 21
Requires:       pcsc-lite
Requires:       opensc

%description pin-pkcs11
Automatically unlocks LUKS block devices through a PKCS#11 device.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%meson -Duser=clevis -Dgroup=clevis
%meson_build

%install
%meson_install
install -p -D -m 0644 %{S:1} %{buildroot}%{_sysusersdir}/clevis.conf

%check
# add test for clevis-luks-udisks2.desktop: validates the clevis-luks-udisks2.desktop 
# and prints warnings/errors about desktop entry specification violations
desktop-file-validate %{buildroot}%{_sysconfdir}/xdg/autostart/%{name}-luks-udisks2.desktop
%meson_test

%pre
%sysusers_create_compat %{S:1}
# Add clevis user to tss group.
if getent group tss >/dev/null && ! groups %{name} | grep -q "\btss\b"; then
    usermod -a -G tss %{name} &>/dev/null
fi
exit 0

%files
%license COPYING*
%{_datadir}/bash-completion/*
%{_bindir}/%{name}-decrypt-tang
%{_bindir}/%{name}-decrypt-tpm2
%{_bindir}/%{name}-decrypt-sss
%{_bindir}/%{name}-decrypt-null
%{_bindir}/%{name}-decrypt
%{_bindir}/%{name}-encrypt-tang
%{_bindir}/%{name}-encrypt-tpm2
%{_bindir}/%{name}-encrypt-sss
%{_bindir}/%{name}-encrypt-null
%{_bindir}/%{name}
%{_bindir}/%{name}-luks-unlock
%{_bindir}/%{name}-luks-unbind
%{_bindir}/%{name}-luks-bind
%{_bindir}/%{name}-luks-common-functions
%{_bindir}/%{name}-luks-list
%{_bindir}/%{name}-luks-edit
%{_bindir}/%{name}-luks-regen
%{_bindir}/%{name}-luks-report
%{_bindir}/%{name}-luks-pass
%{_sysusersdir}/clevis.conf

%files systemd
%{_libexecdir}/%{name}-luks-askpass
%{_libexecdir}/%{name}-luks-unlocker
%{_unitdir}/%{name}-luks-askpass.path
%{_unitdir}/%{name}-luks-askpass.service

%files dracut
%dir %{_prefix}/lib/dracut/modules.d/60%{name}
%{_prefix}/lib/dracut/modules.d/60%{name}/clevis-hook.sh
%{_prefix}/lib/dracut/modules.d/60%{name}/module-setup.sh
%dir %{_prefix}/lib/dracut/modules.d/60%{name}-pin-null
%{_prefix}/lib/dracut/modules.d/60%{name}-pin-null/module-setup.sh
%dir %{_prefix}/lib/dracut/modules.d/60%{name}-pin-sss
%{_prefix}/lib/dracut/modules.d/60%{name}-pin-sss/module-setup.sh
%dir %{_prefix}/lib/dracut/modules.d/60%{name}-pin-tang
%{_prefix}/lib/dracut/modules.d/60%{name}-pin-tang/module-setup.sh
%dir %{_prefix}/lib/dracut/modules.d/60%{name}-pin-tpm2
%{_prefix}/lib/dracut/modules.d/60%{name}-pin-tpm2/module-setup.sh

%files udisks2
%{_sysconfdir}/xdg/autostart/clevis-luks-udisks2.desktop
%attr(4755, root, root) %{_libexecdir}/clevis-luks-udisks2

%files pin-pkcs11
%{_libexecdir}/%{name}-luks-pkcs11-askpass
%{_libexecdir}/%{name}-luks-pkcs11-askpin
%{_bindir}/%{name}-decrypt-pkcs11
%{_bindir}/%{name}-encrypt-pkcs11
%{_bindir}/%{name}-pkcs11-afunix-socket-unlock
%{_bindir}/%{name}-pkcs11-common
%{_unitdir}/%{name}-luks-pkcs11-askpass.service
%{_unitdir}/%{name}-luks-pkcs11-askpass.socket
%dir %{_prefix}/lib/dracut/modules.d/60%{name}-pin-pkcs11
%{_prefix}/lib/dracut/modules.d/60%{name}-pin-pkcs11/module-setup.sh
%{_prefix}/lib/dracut/modules.d/60%{name}-pin-pkcs11/%{name}-pkcs11-hook.sh
%{_prefix}/lib/dracut/modules.d/60%{name}-pin-pkcs11/%{name}-pkcs11-prehook.sh

%files help
%{_mandir}/man?/*

%changelog
* Thu Nov 14 2024 Funda Wang <fundawang@yeah.net> - 21-1
- update to 21
- create user and group in pre section

* Mon Jul 15 2024 dillon chen <dillon.chen@gmail.com> - 20-1
- update setools to 20

* Wed Feb 8 2023 dillon chen <dillon.chen@gmail.com> - 18-2
- buildlrequires compat-openssl11 after openeel upgrade to 3.0

* Tue Jul 27 2021 wangchen <wangchen137@huawei.com> - 18-1
- Update version to 18

* Mon Jul 19 2021 yixiangzhike <zhangxingliang3@huawei.com> - 15-2
- Delete unnecessary gdb from BuildRequires

* Fri Oct 30 2020 panxiaohe <panxiaohe@huawei.com> - 15-1
- Update to v15

* Mon May 25 2020 openEuler Buildteam <buildteam@openeuler.org> - 11-4
- Rebuild for clevis

* Fri Oct 18 2019 openEuler Buildteam <buildteam@openeuler.org> - 11-3
- Add COPYING.openssl

* Wed Sep 25 2019 openEuler Buildteam <buildteam@openeuler.org> - 11-2
- Adjust requires

* Tue Sep 10 2019 openEuler Buildteam <buildteam@openeuler.org> - 11-1
- Package init

